# ForgeRock Docker Stack #

## Overview ##
In order to create a clear separation and to maximize reusing the components, the Indigo Docker Stack as been broken as follow

**Base Images**

* Third party images and binaries
* These images contains the building block such as Tomcat, Java and basic utilities as well as the 	ForgeRock binaries, they are fetched from ForgeRock Maven repository.
* Given they only contain publicly available bits, these images are public, and can be downloaded by anyone.
* Currently, builds are automated through DockerHub, any changes to the sources will cause a new build to be created.

**FR16 Images**

* Indigo IP
* These images are based on the 2016 ForgeRock Release, namely OpenAM13, OpenDJ3, OpenIG4 and OpenIDM4
* The sources are (currently) hosted on a private bitbucket repository
* The Docker Image build are (currently) not hosted on Docker Hub and must be built on the target machine.
* The images contain templated configuration files.

## Getting Started ##
Choose a base location to host the Docker Stack related files

	mkdir Docker
	cd Docker

Initialize and clone the GIT project with all the Docker Files

	git init
	git remote add origin https://bitbucket.org/nseigneur/indigolabs.git
	git clone https://bitbucket.org/nseigneur/indigolabs.git

Start the Indigo Docker Stack

	cd indigolabs/fr16
	source indigolabs.env [You can modify or clone this file to configure your own variables]
	docker-compose pull [Will download from DockerHub, skipping this step will build images locally]
	docker-compose up -d

Fix host file based on DOMAIN variable configured in indigolabs.env

	127.0.0.1 iam.indigolabs.ca
	127.0.0.1 openig.indigolabs.ca
	127.0.0.1 http.indigolabs.ca
	127.0.0.1 openam.indigolabs.ca
	127.0.0.1 opendj.indigolabs.ca
	127.0.0.1 openidm.indigolabs.ca

Navigate to landing page

	http://iam.indigolabs.ca
