# OpenIG #

## Features Overview ##

* Default configuration and routing to the WebApp "puclic" area
* PEP route to perform policy enforcement and response attribute retrieval
* SAML SP configuration and routes

## Important files and locations in the container ##

* Validate Discovery is ready: /etc/bootstrap/bin/testReady.sh
* OpenIG Configuration script: /etc/openig/bin/configure.sh
* OpenIG Templates: /etc/openig/templates/
* OpenIG Configurations: /root/.openig/config
* OpenIG SAML SP files: /root/.openig/SAML
