#!/usr/bin/env bash
	echo "Creating an new OpenIG instances"

#Validate the Discovery layer is available and ready - Retry 12 times wait 5 seconds between tries
    source /etc/bootstrap/bin/testReady.sh discovery 12 5

#Validate OpenAM is available and ready - Retry 30 times wait 12 seconds between tries
    source /etc/bootstrap/bin/testReady.sh openam 30 12

echo "Create tomcat-user.xml with master password"
	consul-template -consul consul:8500 -once -template "/etc/openig/templates/tomcat-users.ctmpl:/opt/tomcat/conf/tomcat-users.xml"

echo "Creating OpenIG JSON configs from Templates"
	consul-template -consul consul:8500 -once -template "/etc/openig/templates/config.ctmpl:/root/.openig/config/config.json"
	consul-template -consul consul:8500 -once -template "/etc/openig/templates/routes/04-pep.ctmpl:/root/.openig/config/routes/04-pep.json"
	consul-template -consul consul:8500 -once -template "/etc/openig/templates/routes/05-saml.ctmpl:/root/.openig/config/routes/05-saml.json"
	consul-template -consul consul:8500 -once -template "/etc/openig/templates/routes/05-federate.ctmpl:/root/.openig/config/routes/05-federate.json"

echo "Creating SAML configurations from Templates"
	consul-template -consul consul:8500 -once -template "/etc/openig/templates/SAML/fedlet.ctmpl:/root/.openig/SAML/fedlet.cot"
	consul-template -consul consul:8500 -once -template "/etc/openig/templates/SAML/SAML-OpenAM-idp.ctmpl:/root/.openig/SAML/idp.xml"
	consul-template -consul consul:8500 -once -template "/etc/openig/templates/SAML/SAML-OpenAM-idp-ext.ctmpl:/root/.openig/SAML/idp-extended.xml"
	consul-template -consul consul:8500 -once -template "/etc/openig/templates/SAML/SAML-OpenIG-sp.ctmpl:/root/.openig/SAML/sp.xml"
	consul-template -consul consul:8500 -once -template "/etc/openig/templates/SAML/SAML-OpenIG-sp-ext.ctmpl:/root/.openig/SAML/sp-extended.xml"

echo "Creating OIDC&OAuth2 configurations from Templates"
	consul-template -consul consul:8500 -once -template "/etc/openig/templates/routes/05-openid.ctmpl:/root/.openig/config/routes/05-openid.json"
	consul-template -consul consul:8500 -once -template "/etc/openig/templates/routes/05-oauth-rs.ctmpl:/root/.openig/config/routes/05-oauth-rs.json"
