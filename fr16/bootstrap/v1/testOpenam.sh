#!/usr/bin/env bash

# Call this script to see if OpenAM is ready.
# Call with the number of retries as well as sleep time (seconds).
# sh scriptname 1
    if [ $# -ne 3 ]
    then
      echo "Usage: `basename $0` <domain> <retries> <waittime>"
      exit $E_BADARGS
    fi

#Map arguments to variables
  domain=$1
  retries=$2
  waittime=$3

#Parameters passed:
    echo "$0 will execute $retries times with a sleep time of $waittime for a total of: $(($retries * $waittime)) seconds"

#Make sure LDAP directory is available
#Search for the a load balancer account in the LDAP - doing application level validation that Directory is ready
    ((count = $retries))
    while [[ $count -ne 0 ]] ; do
        openamstatus=`curl --head --silent http://openam.$domain:8080/openam/isAlive.jsp 2>/dev/null | head -n 1 | cut -d$' ' -f2`
        if [[ $openamstatus != "200" ]];
            then
                echo "Access Management at http://openam.$domain:8080/openam/isAlive.jsp not ready - counter: $count - time remaining: $(($count * $waittime)) seconds";
                sleep $waittime;
            else
                echo "Successfully connected to http://openam.$domain:8080/openam/isAlive.jsp";
                ((count = 1));
        fi
        ((count = count - 1));
    done

#Empty the variables used in the script
  unset domain
  unset retries
  unset waittime
  unset count
