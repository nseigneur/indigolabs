# Bootstrap container #

## Features Overview ##

* Single configuration entrypoint for the whole stack
* Expects environment variables containing the configurable aspects of the stack to be passed
* Ephemeral container - starts, bootstraps the K/V and then exit

## Important files and locations in the container ##

* Validate Discovery is ready: /etc/bootstrap/bin/testReady.sh
* Bootstrap the Key/Value store with environement variables: /etc/bootstrap/bin/run.sh
