#!/usr/bin/env bash

# Call this script to see if Directory is ready.
# Call with the number of retries as well as sleep time (seconds).
# sh scriptname 1 2 3 4
    if [ $# -ne 4 ]
    then
      echo "Usage: `basename $0` <password> <basedn> <retries> <waittime>"
      exit $E_BADARGS
    fi

#Map arguments to variables
  directorypassword=$1
  basedn=$2
  retries=$3
  waittime=$4

#Parameters passed:
    echo "$0 will execute $retries times with a sleep time of $waittime for a total of: $(($retries * $waittime)) seconds"

#Make sure LDAP directory is available
#Search for the a load balancer account in the LDAP - doing application level validation that Directory is ready
    ((count = $3))
    while [[ $count -ne 0 ]] ; do
        directoryready=`ldapsearch -h opendj -p 389 -b ou=services,$basedn -s one -d 0 -D "cn=Directory Manager" -w  $directorypassword "(uid=lb)" uid|grep numEntries|cut -d ' ' -f3`;
        if [[ $directoryready != "1" ]];
            then
                echo "Directory at opendj:389 not ready - counter: $count - time remaining: $(($count * $waittime)) seconds";
                sleep $waittime;
            else
                echo "Successfully connected to opendj:389 and found lb account";
                ((count = 1));
        fi
        ((count = count - 1));
    done

#Empty the variables used in the script
    unset directorypassword
    unset basedn
    unset count
    unset retries
    unset waittime
