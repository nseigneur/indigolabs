#!/usr/bin/env bash
	echo "Creating an new OpenIDM instance"

#Validate the Discovery layer is available and ready - Retry 12 times wait 5 seconds between tries
    source /etc/bootstrap/bin/testReady.sh discovery 12 5

#Validate OpenDJ is available and ready - Retry 30 times wait 12 seconds between tries
    source /etc/bootstrap/bin/testReady.sh opendj 30 12

echo "Creating OpenIDM JSON configs from Templates"
	consul-template -consul consul:8500 -once -template "/etc/openidm/templates/DataLoad.ctmpl:/opt/openidm/data/DataLoad.csv"
	consul-template -consul consul:8500 -once -template "/etc/openidm/templates/provisioner.openicf-ldap.ctmpl:/opt/openidm/conf/provisioner.openicf-ldap.json"
	consul-template -consul consul:8500 -once -template "/etc/openidm/templates/sync.ctmpl:opt/openidm/conf/sync.json"
