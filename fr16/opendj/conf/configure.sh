#!/usr/bin/env bash
	echo "Instance data Directory is empty. Creating new DJ instance"

#Validate the Discovery layer is available and ready - Retry 12 times every 5 seconds
	source /etc/bootstrap/bin/testReady.sh discovery 12 5

#TODO - Find why we can't move *
#In order to persist OpenDJ information, we must mount a volume - This location has to be empty in container at mount
#Since we already had the binaries under /opt/opendj, at build time, we move binaries to /var/tmp/opendj allowing the mount
#As this run's on the built image, we can move back the binaries to the new mounted volume.

echo "Moving back OpenDJ binaries"
	mv /var/tmp/opendj/bin/ /opt/opendj/
	mv /var/tmp/opendj/lib/ /opt/opendj/
	mv /var/tmp/opendj/template/ /opt/opendj/
	mv /var/tmp/opendj/setup /opt/opendj/
	mv /var/tmp/opendj/uninstall /opt/opendj/
	mv /var/tmp/opendj/upgrade /opt/opendj/

#Using consul-template to replace all variables.
#We prefix the DIT and ACI to make sure they run sequentially.
echo "Creating LDIFs from Templates"
	#Make sure it runs first
	consul-template -consul consul:8500 -once -template "/etc/opendj/templates/0_DIT-ACI.ctmpl:/etc/opendj/ldif/0_DIT-ACI.ldif"
	consul-template -consul consul:8500 -once -template "/etc/opendj/templates/opendj_uma_audit.ctmpl:/etc/opendj/ldif/opendj_uma_audit.ldif"
	consul-template -consul consul:8500 -once -template "/etc/opendj/templates/opendj_uma_pending_requests.ctmpl:/etc/opendj/ldif/opendj_uma_pending_requests.ldif"
	consul-template -consul consul:8500 -once -template "/etc/opendj/templates/opendj_uma_resource_sets.ctmpl:/etc/opendj/ldif/opendj_uma_resource_sets.ldif"
	consul-template -consul consul:8500 -once -template "/etc/opendj/templates/opendj_uma_resource_set_labels.ctmpl:/etc/opendj/ldif/opendj_uma_resource_set_labels.ldif"
	consul-template -consul consul:8500 -once -template "/etc/opendj/templates/opendj_user_init.ctmpl:/etc/opendj/ldif/opendj_user_init.ldif"
	consul-template -consul consul:8500 -once -template "/etc/opendj/templates/users.ctmpl:/etc/opendj/ldif/users.ldif"
	#Add zz_ to make sure it runs last as it used by other container to make sure OpenDJ is configured and ready
	consul-template -consul consul:8500 -once -template "/etc/opendj/templates/zz_services.ctmpl:/etc/opendj/ldif/zz_services.ldif"

echo "Creating Master password from template"
	consul-template -consul consul:8500 -once -template "/etc/opendj/templates/dm-pw.ctmpl:/etc/opendj/secrets/dm-pw"

echo "Creating Configuration"
	consul-template -consul consul:8500 -once -template "/etc/opendj/templates/opendj.ctmpl:/etc/opendj/conf/opendj.props"

echo "Executing OpenDJ Setup"
	/opt/opendj/setup --cli --propertiesFilePath /etc/opendj/conf/opendj.props --acceptLicense --no-prompt

#Using Prefixing trick above ensure that the Loop import sequentially while avoiding to do individual commands
echo "Import all LDIFs required for configuration"
  for file in /etc/opendj/ldif/*;  do
  	  echo "Importing $file"
      /opt/opendj/bin/ldapmodify --hostname localhost --port 389 -a --filename $file --bindDN "cn=Directory Manager" --bindPasswordFile /etc/opendj/secrets/dm-pw
      echo ""
  done

#Get BaseDN - Required to rebuild indexes
	BASE_DN=`curl --silent http://consul:8500/v1/kv/platform/global/basedn|jq '.[0].Value' --raw-output|base64 --decode`

echo "Rebuilding $BASE_DN indexes"
	/opt/opendj/bin/rebuild-index --port 4444  --hostname localhost --bindDN "cn=Directory Manager" --bindPasswordFile /etc/opendj/secrets/dm-pw --baseDN $BASE_DN --rebuildDegraded --trustall
