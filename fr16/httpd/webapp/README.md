# WebApp Apache WebServer container #

## Features Overview ##

* Simple web application that provide "public" and "private" directories.
* Used to POC federation, PolicyEnforcementPoint and OAuth/OIDC through OpenIG.

## Important files and locations in the container ##

* Validate Discovery is ready: /etc/bootstrap/bin/testReady.sh
* Configure instance: /etc/httpd/bin/configure.sh
* Apache FrontEnd configuration: /etc/httpd/conf/httpd.conf
* Public unprotected page: /var/www/html/index.html
* Private protected page: /var/www/html/private/index.html
