#!/bin/bash

# Make sure we're not confused by old, incompletely-shutdown httpd
# context after restarting the container.  httpd won't start correctly
# if it thinks it is already running.
rm -rf /run/httpd/* /tmp/httpd*

#Bootstrap Web Server
/etc/httpd/bin/configure.sh

#Start HTTP server in FOREGROUND
exec /usr/sbin/apachectl -DFOREGROUND