# FrontEnd Apache WebServer container #

## Features Overview ##

* Web server that provide access to web resources through port 80 using on the host machine
* Provide Landing page with links for easy access to stack feature
* ProxyPass for OpenIDM, OpenIG and OpenAM

## Important files and locations in the container ##

* Validate Discovery is ready: /etc/bootstrap/bin/testReady.sh
* Configure instance: /etc/httpd/bin/configure.sh
* Apache FrontEnd configuration: /etc/httpd/conf/httpd.conf
* Landing page for stack: /var/www/html/index.html
