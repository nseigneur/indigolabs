#!/usr/bin/env bash
echo "Bootstrap and configure httpd instance."

#Validate the Discovery layer is available and ready - Retry 12 times wait 5 seconds between tries
    source /etc/bootstrap/bin/testReady.sh discovery 12 5

#TODO - The tomcat user file will not take place until Tomcat restart
#Create files from templates
    consul-template -consul consul:8500 -once -template "/etc/httpd/templates/index.ctmpl:/var/www/html/index.html"
    consul-template -consul consul:8500 -once -template "/etc/httpd/templates/status.ctmpl:/var/www/html/status.js"
    consul-template -consul consul:8500 -once -template "/etc/httpd/templates/http.ctmpl:/etc/httpd/conf/httpd.conf"
